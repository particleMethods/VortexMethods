import numpy as np
import matplotlib.pyplot as plt
from numba import jit
import pdb


#def gen_v(z,strength):
#    n   = z.shape[0]
#    Z   = np.reshape(z,[1,n]) + np.zeros([n,n])
#    num = -1j*np.reshape(strength,[1,n]) 
#    den = 2*np.pi*(Z-Z.T)
#    V   = np.divide(num,den, out = np.zeros_like(den), where=den!=0)
#    return np.sum(V.conjugate(),axis=1)
#


def gen_v(z_source, z_moving, source_strength):
    n   = z_source.shape[0]
    m   = z_moving.shape[0]
    Z   = np.reshape(z_source,[1,n]) - np.reshape(z_moving,[m,1])
    num = -1j*np.reshape(source_strength,[1,n])
    den = 2*np.pi*(Z)
    V   = np.divide(num,den, out = np.zeros_like(den), where=den!=0.0)

    return np.sum(V.conjugate(),axis=1)

class System(object):
    def __init__(self):
        self.z          = np.array([])
        self.strength   = np.array([])
        self.v          = np.array([])

    def add_vortex(self,strength=1,z=np.complex(0,0), v=np.complex(0,0)):
        self.z = np.append(self.z, z)
        self.v = np.append(self.v, v)
        self.strength = np.append(self.strength, strength)

    def euler(self,z_source, z_moving ,dt):
        return gen_v(z_source, z_moving, self.strength)*dt

    def RK2(self,z_source,z_moving dt):
        zmid = self.euler(z_source, z_moving, dt/2.0)
        return gen_v(z_source + zmid, self.strength)*dt,

    def simulate(self, dt, numSteps, fn):
        self.Z = [self.z.copy()]
        for step in xrange(numSteps):
            #self.ztracers = self.ztracers + fn(self.z,self.ztracers,dt) 
            self.dz        = fn(self.z,self.ztracers,dt)
            self.z         = self.z + self.dz[0]
            self.ztracers  = self.ztracers + self.dz[1]
            self.Z.append(self.z)
