import numpy as np
import matplotlib.pyplot as plt

class particle:
    def __init__(self, x, y, fixed=False):
        self.x, self.y = x, y
        self.fixed = fixed
        if not fixed:
            self.X = [self.x]
            self.Y = [self.y]

    def get_vel(self,x,y):
        return 0

    def get_Phi(self, x, y):
        return 0

    def move(self, dx, dy):
        if not self.fixed:
            self.x, self.y = self.x + dx, self.y + dy
            self.X.append(self.x)
            self.Y.append(self.y)
        return

class tracer(particle):
    def get_vel(self, x, y):
        return 0

class uniform(particle):
    def __init__(self, strength = 1, alpha = 0, fixed=True):
        self.fixed = fixed
        self.strength = strength
        self.alpha = alpha
    def get_vel(self, x, y):
        out = np.zeros_like(x)
        out = np.append([out],[out],0)
        out[0] = self.strength*np.cos(self.alpha)
        out[1] = self.strength*np.sin(self.alpha)
        return out
    def get_Phi(self, x, y):
        z =  self.strength*(np.e**(-1j*self.alpha))*(x+1j*y)
        return np.array([np.real(z),np.imag(z)])

class source(particle):
    def __init__(self, x, y, strength=1, fixed=True):
        particle.__init__(self,x,y,fixed)
        self.strength = strength
    def get_vel(self, x, y):
        xi, yi = self.x, self.y
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        return self.strength*np.array([x,y])/(2*np.pi*r_sq)
    def get_Phi(self, x, y):
        xi, yi = self.x, self.y
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        return self.strength*np.array([np.log(np.sqrt(r_sq)),np.arctan(y/x)])/(2*np.pi)

class sink(particle):
    def __init__(self, x, y, strength=1, fixed=True):
        self.snk = source(x,y,-1*strength,fixed)
    def get_vel(self, x, y):
        return self.snk.get_vel(x, y)
    def get_Phi(self, x, y):
        return self.snk.get_Phi(x, y)

class vortex(particle):
    def __init__(self, x, y, strength=1, fixed=False):
        particle.__init__(self,x,y,fixed)
        self.strength = strength
    def get_vel(self, x, y):
        xi, yi = self.x, self.y
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        #if r_sq == 0:
        #    return 0
        #else:
        return self.strength*np.array([-y,x])/(2*np.pi*r_sq)
    def get_Phi(self, x, y):
        xi, yi = self.x, self.y
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        return self.strength*np.array([np.arctan(y/x), -np.log(np.sqrt(r_sq))])/(2*np.pi)

class system:
    def __init__(self, particles=None):
        self.particles=[]
        if particles is not None:
            self.particles = particles

    def add_particle(self, particle):
        self.particles.append(particle)
    
    def add_particles(self, particles):
        self.particles = self.particles + particles
    
    def get_vel(self, x,y, exc_particle=None):
        vel = 0
        for particle in [particle for particle in self.particles if particle is not exc_particle]:
            vel = vel + particle.get_vel(x, y)
        return vel

    def get_Phi(self, x,y):
        Phi = 0
        for particle in self.particles:
            Phi = Phi + particle.get_Phi(x, y)
        return Phi

    def get_moving_particles(self):
        return [particle for particle in self.particles if not particle.fixed]

    def get_tracers(self):
        return [particle for particle in self.particles if particle.__class__.__name__=="tracer"]
    def step(self, dt, method="euler"):
        if method == "euler":
            for particle in self.get_moving_particles():
                particle.dx, particle.dy = dt*self.get_vel(particle.x, particle.y)

        elif method == "RK4":
            for particle in self.get_moving_particles():
                x, y = particle.x, particle.y
                k1x, k1y = self.get_vel(x,y, particle)
                k2x, k2y = self.get_vel(x+dt*k1x/2, y+dt*k1y/2, particle)
                k3x, k3y = self.get_vel(x+dt*k2x/2, y+dt*k2y/2, particle)
                k4x, k4y = self.get_vel(x+dt*k3x, y+dt*k3y, particle)

                dx = dt*(k1x+2*k2x+2*k3x+k4x)/6
                dy = dt*(k1y+2*k2y+2*k3y+k4y)/6
                particle.dx, particle.dy = dx, dy

        elif method == "RK2":
            #Using midpoint method
            for particle in self.get_moving_particles():
                x, y = particle.x, particle.y
                k1x, k1y = self.get_vel(x,y, particle)
                k2x, k2y = self.get_vel(x+dt*k1x/2, y+dt*k1y/2, particle)

                dx = dt*k2x
                dy = dt*k2y

                particle.dx, particle.dy = dx, dy

        for particle in self.get_moving_particles():
            particle.move(particle.dx, particle.dy)

    def simulate(self, dt, Th, method="euler"):
        tsteps = int(Th/dt)
        for n in xrange(tsteps):
            self.step(dt, method)
