import numpy as np

def panel_vel_10(z1,z2,z):
    return (((z/l) - 1)*np.log((z-l)/z) + 1)

def cent_pan_vel(z, gamma1, gamma2, l):
    #Gives the velocity at z due to a panel at origin oriented along x of length l
    temp1 = ((z/l) - 1)*np.log((z-l)/z) + 1
    temp2 = (z/l)*np.log(1-l/z) + 1
    v = -1j*(gamma1*temp1 - gamma2*temp2)/(2*np.pi)
    return v.conjugate()


def panel_velocity(z1,z2,gamma1,gamma2,z):
    #Gives the velocit at z due to a panel with ends at z1 and z2
    l = np.absolute(z2-z1)
    t = (z2-z1)/l
    z = (z-z1)/t
    return cent_pan_vel(z,gamma1,gamma2, l)*t

def vortex_vel(z, gamma):
    #Returns velocity at z due to a vortex at origin
    v = -1j*gamma/(2*np.pi*z)
    return v.conjugate()


#mapping = {
#        "vortex"    : vortex_vel
#        "panel"     : panel_velocity
#        "uniform"   : uniform
#}
