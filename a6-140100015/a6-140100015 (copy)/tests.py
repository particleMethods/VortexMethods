import unittest
import main

class vortexTest(unittest.TestCase):
    def test_point_vel(self):
        vortex = main.vortex(complex(1.0,1.0),strength=2.0, blob_type="point")

        vel    = vortex.get_vel(complex(2,1))
        self.assertEqual(vel.real,0.0, "Incorrect direction")
        self.assertGreater(vel.imag,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

        vel    = vortex.get_vel(complex(0,1))
        self.assertEqual(vel.real,0.0, "Incorrect direction")
        self.assertLess(vel.imag,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

        vel    = vortex.get_vel(complex(1,0))
        self.assertEqual(vel.imag,0.0, "Incorrect direction")
        self.assertGreater(vel.real,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

        vel    = vortex.get_vel(complex(1,2))
        self.assertEqual(vel.imag,0.0, "Incorrect direction")
        self.assertLess(vel.real,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

    def test_chorin_vel_outside(self):
        vortex = main.vortex(complex(1.0,1.0),strength=2.0, blob_type="chorin", d=0.1)

        vel    = vortex.get_vel(complex(2,1))
        self.assertEqual(vel.real,0.0, "Incorrect direction")
        self.assertGreater(vel.imag,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

        vel    = vortex.get_vel(complex(0,1))
        self.assertEqual(vel.real,0.0, "Incorrect direction")
        self.assertLess(vel.imag,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

        vel    = vortex.get_vel(complex(1,0))
        self.assertEqual(vel.imag,0.0, "Incorrect direction")
        self.assertGreater(vel.real,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

        vel    = vortex.get_vel(complex(1,2))
        self.assertEqual(vel.imag,0.0, "Incorrect direction")
        self.assertLess(vel.real,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/main.np.pi, "Incorrect value")

    def test_chorin_vel_inside(self):
        vortex = main.vortex(complex(1.0,1.0),strength=2.0, blob_type="chorin", d=2.0)

        vel    = vortex.get_vel(complex(2,1))
        self.assertEqual(vel.real,0.0, "Incorrect direction")
        self.assertGreater(vel.imag,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/(2.0*main.np.pi), "Incorrect value")

        vel    = vortex.get_vel(complex(0,1))
        self.assertEqual(vel.real,0.0, "Incorrect direction")
        self.assertLess(vel.imag,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/(2.0*main.np.pi), "Incorrect value")

        vel    = vortex.get_vel(complex(1,0))
        self.assertEqual(vel.imag,0.0, "Incorrect direction")
        self.assertGreater(vel.real,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/(2.0*main.np.pi), "Incorrect value")

        vel    = vortex.get_vel(complex(1,2))
        self.assertEqual(vel.imag,0.0, "Incorrect direction")
        self.assertLess(vel.real,0.0,"Incorrect sign for vortex velocity")
        self.assertEqual(abs(vel),1.0/(2.0*main.np.pi), "Incorrect value")
    
    def test_uniform(self):
        u = main.uniform(strength=complex(2.0,2.0))
        self.assertEqual(u.get_vel(complex(2.0,7.0),complex(2.0,2.0)))
 
if __name__ == '__main__':
        unittest.main(verbosity=2)
