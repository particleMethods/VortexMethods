from particles import uniform,vortex
from panels import hat_panel, body
import numpy as np
import matplotlib.pyplot as plt
import math
import cmath
import pdb;


np.random.seed(0)

class system:
    def __init__(self, nu=1e-3, gamma_max=0.1, particles=None, bodies=None):
        self.particles  = []
        self.bodies     = []
        self.panels     = []
        self.nu         = nu
        self.gamma_max  = gamma_max


        if particles is not None:
            self.particles = particles
        if bodies is not None:
            self.bodies = bodies


    def add_particle(self, particle):
        self.particles.append(particle)
    
    def add_particles(self, particles):
        self.particles = self.particles + particles
    
    def add_body(self, body):
        self.bodies.append(body)
    
    def add_bodies(self, bodies):
        self.bodies = self.bodies + bodies

    def get_vel(self, z, inc_panel=False, src_particles = None, exc_particle=None):
        if src_particles is None:
            src_particles = self.particles
        if inc_panel:
            src_particles = self.particles + self.panels
        vel = 0
        #for particle in [particle for particle in self.particles if particle is not exc_particle]:
        for particle in src_particles:
            vel = vel + particle.get_vel(z)
#        for body in self.bodies:
#            for panel in body.panels:
#                vel = vel + panel.get_vel(z)
        return vel
    def computeA(self):
        a = []
        A = []
        n = 0
        panels = []
        control_points = []

        for body in self.bodies:
            panels = panels + body.panels
            control_points = control_points + body.cps

        self.control_points = control_points
        self.panels         = panels
        p, n = len(panels), len(control_points)
        for cp in control_points:
            for panel in panels:
                temp = panel.specific_velocity(cp.z).conjugate()*cp.n
                a.append(temp.real)
            A.append(a)
            a = []

        self.A = np.array(A)
        self.A = np.concatenate([self.A,np.ones([1,p])], axis=0)
    
    def computeB(self):
        b = []
        for cp in self.control_points:
            temp = (cp.body.v-1*self.get_vel(cp.z))*cp.n.conjugate()
            b.append(temp.real)
        b.append(0)
        self.B = np.array(b)

    def assignGammas(self):
        gammas = np.linalg.lstsq(self.A,self.B)[0].tolist()
        for i, panel in enumerate(self.panels):
            panel.gamma = gammas[i]

    def solvePanel(self):
        self.computeA()
        self.computeB()
        self.assignGammas()

    def get_Phi(self, z):
        #TODO
        assert False
        Phi = 0
        for particle in self.particles:
            Phi = Phi + particle.get_Phi(x, y)
        return Phi

    def get_moving_particles(self):
        return [particle for particle in self.particles if not particle.fixed]

    def get_tracers(self):
        return [particle for particle in self.particles if particle.__class__.__name__=="tracer"]
    
    def RK2_step1(self, dt):
        for particle in self.get_moving_particles():
            particle.temp_z = particle.z
            particle.dz = self.get_vel(particle.z, inc_panel=True)*dt/2

        for particle in self.get_moving_particles():
            particle.move(particle.dz)
    
    def RK2_step2(self, dt):
        for particle in self.get_moving_particles():
            particle.dz = self.get_vel(particle.z, inc_panel=True)*dt

        for particle in self.get_moving_particles():
            particle.z = particle.temp_z + particle.dz

    def advect(self,dt):
        #self.solvePanel()
        self.RK2_step1(dt)
        self.solvePanel()
        self.RK2_step2(dt)

    def diffuse(self,dt):
        for particle in self.get_moving_particles():
            if abs(particle.strength) > self.gamma_max:
                num_new_vortices                    = int(abs(particle.strength/self.gamma_max))+1
                strength_new                        = particle.strength/num_new_vortices
                z_new, type_new, new_d, new_style   = particle.z, particle.blob_type, particle.d, particle.style
                new_vortices                        = [vortex(z_new,strength_new, blob_type=type_new, d=new_d, style = new_style) for i in xrange(num_new_vortices)]
                self.add_particles(new_vortices)
                self.particles.remove(particle)
                #Check

        for particle in self.get_moving_particles():
            displacements = np.random.normal(0.0,np.sqrt(2*self.nu*dt),(2,))
            particle.dz = complex(displacements[0], displacements[1])
            particle.move(particle.dz)

    def satisfy_no_slip(self):
        for body in self.bodies:
            for i, panel in enumerate(body.panels):
                unit_normal = 1.0j*panel.ta
                z_blob      = 0.5*(panel.z+panel.za) + unit_normal*panel.la/np.pi
                gamma       = 1.0*(panel.gamma + body.panels[i-1].gamma)*panel.la/2.0
                
                self.add_particle(vortex(z_blob,gamma,blob_type="chorin", d=panel.la/np.pi, style='rx'))
                
    def reflect(self,r):
        for particle in self.get_moving_particles():
            rz = abs(particle.z)
            if rz-r<0:
                r_vect = particle.z/rz
                particle.z = (2.0*r - rz)*r_vect


    def step(self, dt):
        #pdb.set_trace()
        self.advect(dt)
        self.solvePanel()
        self.satisfy_no_slip()
        self.diffuse(dt)
        self.reflect(1.0)
        self.solvePanel()


    def plot(self, ax):
        for particle in self.particles:
            particle.plot(ax)
        for body in self.bodies:
            body.plot(ax)
    def plot_vel_field(self,x1,y1,x2,y2,nx,ny,ax):
        x = np.linspace(x1,x2,nx)
        y = np.linspace(y1,y2,ny)
        X,Y = np.meshgrid(x,y)
        Z = X + 1j*Y
        V = np.zeros_like(Z).tolist()
        Z = Z.tolist()
        for i in xrange(ny):
            for j in xrange(nx):
                V[i][j] = self.get_vel(Z[i][j],inc_panel=True)

        ax.quiver(X,Y,np.real(V), np.imag(V))


    def plot_vorticity(self,x1,y1,x2,y2,nx,ny,ax):
        x   = np.linspace(x1,x2,nx)
        y   = np.linspace(y1,y2,ny)
        b,h = x[1]-x[0],y[1]-y[0]
        X,Y = np.meshgrid(x,y)
        V   = np.zeros_like(X).tolist()
	
	for vortex in self.get_moving_particles():
            try:
                xv, yv    = vortex.z.real, vortex.z.imag
                i, j    = int((yv-y1)/h), int((xv-x1)/b)
                xe,ye   = xv-x[j], yv - y[i]
                factor  = vortex.strength/(h*b)

                V[i][j]      += (h - ye)*(b-xe)*factor
                V[i+1][j]    += (ye)*(b-xe)*factor
                V[i][j+1]    += (h-ye)*(xe)*factor
                V[i+1][j+1]  += ye*xe*factor
            except:
                continue
    
        ax.contourf(x,y,V)
        #pdb.set_trace()
        return

    def simulate(self, dt, Th, method="euler"):
        tsteps = int(Th/dt)
        for n in xrange(tsteps):
            self.step(dt)

