from particles import particle
import numpy as np
import matplotlib.pyplot as plt
import math
import cmath

def hatvel(l, z):
    if z == 0 or z == l:
        return 0
    v = -1j*(((z/l)-1)*cmath.log(1-l/z)+1)/(2*np.pi)
    return v.conjugate()

class control_point():
    def __init__(self, z, body, l, t):
        self.body = body
        self.z = z
        self.l = l
        self.t = t
        self.n = 1j*t
        body.cps.append(self)

class hat_panel(particle):
    def __init__(self,body,za,z,zb,gamma,fixed=False):
        self.gamma  = gamma
        self.body   = body
        self.za     = za
        self.zb     = zb
        self.z      = z
        self.fixed  = fixed
        la = za - z
        self.la     = math.sqrt(la.real**2 + la.imag**2)
        lb = zb - z
        self.lb     = math.sqrt(lb.real**2 + lb.imag**2)
        self.ta     = (z - za)/self.la
        self.tb     = (z - zb)/self.lb
        body.panels.append(self)

        #Jugaad
        control_point((za+z)/2.0, body, self.la, self.ta)


    def specific_velocity(self, z):
        la = self.la
        lb = self.lb
        ta = self.ta
        tb = self.tb
        z  = z - self.z
        try:
            return hatvel(la, z/ta)*ta + hatvel(lb, z/tb)*tb
        except:
            pdb.set_trace()

    def get_vel(self,z):
        return self.gamma*self.specific_velocity(z)
    def plot(self,ax):
        #import pdb; pdb.set_trace()
        X, Y = [self.za.real,self.z.real], [self.za.imag,self.z.imag]
        if self.gamma>0:
            c = 'b'
        else:
            c = 'r'
        ax.plot(X,Y,c)
        
class body:
    def __init__(self, v):
        self.v      = v
        self.panels = []
        self.cps    = []

    def plot(self,ax):
        for panel in self.panels:
            panel.plot(ax)


