import matplotlib
matplotlib.use("Agg")
from main import *
import matplotlib.pyplot as plt

matplotlib.rcParams["figure.figsize"] = [20,15]
matplotlib.rcParams.update({'font.size': 20})

def prepare_system(n = 64):
    thetas = np.linspace(0,2*np.pi,n+1)[:-1]
    z = np.cos(thetas) + 1j*np.sin(thetas)
    za = np.roll(z,-1).tolist()
    zb = np.roll(z, 1).tolist()
    z  = z.tolist()

    b = body(v=0.0+0.0j)
    panels = [hat_panel(b, za[i], z[i], zb[i], 0.0) for i in xrange(n)]

    s = system(bodies = [b], nu = (2.0)/(1000.0))
    s.add_particle(uniform())

    s.solvePanel()
    return s

def plot(s, name):
    
    fig,axs = plt.subplots(2,1)
    s.plot_vel_field(-2,-2,20,2,40,40,axs[0])
    s.plot(axs[1])
    for ax in axs:
        ax.set_aspect(1)
    fig.suptitle('{0:.1f}s'.format(name*0.1))
    plt.savefig("./results/"+str(name)+".png")
    plt.close(fig)

s = prepare_system(50)
I = np.zeros(300)

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def cd(I):
    dI = I[1:] - I[:-1]
    cd = dI/0.1
    return cd

for i in xrange(300):
    s.step(0.1)
    for vortex in s.get_moving_particles():
        I[i] += vortex.strength*vortex.z.imag
        
    print i

plot(s,300)

t = np.linspace(0.0,4.2,47)
Cd = cd(moving_average(I))
fig,ax = plt.subplots()
ax.plot(t,Cd)
ax.set_xlabel('t')
ax.set_ylabel('Cd')
plt.savefig("cd.png")
