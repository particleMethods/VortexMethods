import numpy as np
import matplotlib.pyplot as plt
import math
import cmath


class particle:
    def __init__(self, z, fixed=False):
        self.z = z
        self.fixed = fixed
        if not fixed:
            self.Z = [self.z]

    def get_vel(self,z):
        return 0

    def get_Phi(self, z):
        return 0

    def move(self, dz):
        if not self.fixed:
            self.z = self.z + self.dz
            self.Z.append(self.z)
        return


class tracer(particle):
    def get_vel(self, z):
        return 0


class uniform(particle):
    def __init__(self, strength = 1.0 + 0.0j, fixed=True):
        self.fixed = fixed
        self.strength = strength

    def get_vel(self, z):
        return self.strength
    def get_Phi(self, z):
        #TODO
        assert False
        pass
    def plot(self,ax):
        pass


class vortex(particle):
    def __init__(self, z, strength=1, fixed=False, blob_type="chorin", d=0.1, style = 'x'):
        particle.__init__(self,z,fixed)
        self.strength = strength
        self.blob_type = blob_type
        self.d = d
        self.style = 'o'
    
    
    def get_vel(self, z):
        if self.blob_type=="point":
            z = z - self.z
            if z == 0:
                return 0
            v = -1j*self.strength/(2*math.pi*z)
            return v.conjugate()
        elif self.blob_type=="krasny":
            #TODO
            assert False
        elif self.blob_type=="chorin":
            z = z - self.z
            if z == complex(0.0,0.0):
                return complex(0.0)
            if abs(z) < self.d:
                z = z*self.d/abs(z)
            v = -1j*self.strength/(2*math.pi*z)
            return v.conjugate()

    
    def get_Phi(self, z):
        z = z - zi
        #TODO
        assert False
    
    
    def plot_params(self, style):
        self.style = style

    
    def plot(self, ax):
        if self.strength>0:
            style = 'b'+self.style
        else:
            style = 'r' + self.style
        ax.plot([self.z.real],[self.z.imag],style, alpha=0.5)
