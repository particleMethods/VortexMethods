import numpy as np
import matplotlib.pyplot as plt
from numba import jit
import pdb


def gen_v(z,strength):
    n   = z.shape[0]
    Z   = np.reshape(z,[1,n]) + np.zeros([n,n])
    num = -1j*np.reshape(strength,[1,n]) 
    den = 2*np.pi*(Z-Z.T)
    V   = np.divide(num,den, out = np.zeros_like(den), where=den!=0.0)
    np.fill_diagonal(V,0.0+0.0j)
    return np.sum(V.conjugate(),axis=1)


@jit
def gen_v2(zv, zobs, strength):
    n   = zv.shape[0]
    m   = zobs.shape[0]
    Z   = np.reshape(zv,[1,n]) - np.reshape(zobs,[m,1])
    num = -1j*np.reshape(strength,[1,n])
    den = 2*np.pi*(Z)
    V   = np.divide(num,den, out = np.zeros_like(den), where=den!=0.0)

    return np.sum(V.conjugate(),axis=1)

class System(object):
    def __init__(self):
        self.z          = np.array([])
        self.strength   = np.array([])
        self.v          = np.array([])
        self.ztracers   = np.array([])
        self.Z          = np.array([])
        self.type       = []
        self.mapping    = {"Tracer": self.ztracers, "Vortex": self.z}
    def add_tracer(self,z):
        self.ztracers = np.append(self.ztracers, z)
        self.Z = np.append(self.Z, z)
        self.type.append("Tracer")
    def add_vortex(self,strength=1,z=np.complex(0,0), v=np.complex(0,0)):
        self.z = np.append(self.z, z)
        self.v = np.append(self.v, v)
        self.strength = np.append(self.strength, strength)
        self.Z = np.append(self.Z, z)
        self.type.append("Vortex")
    def euler(self,z_active,z_passive,dt):
        #return gen_v(z_active, self.strength)*dt, gen_v2(z_active, z_passive, self.strength)
        return gen_v2()
    def RK2(self,z_active,z_passive,dt):
        zmid = self.euler(z_active, z_passive, dt/2.0)
        return gen_v(z_active + zmid[0], self.strength)*dt, \
               gen_v2(z_active + zmid[0], z_passive + zmid[1], self.strength)*dt

    def simulate(self, dt, numSteps, fn):
        self.Z = [self.z.copy()]
        for step in xrange(numSteps):
            #self.ztracers = self.ztracers + fn(self.z,self.ztracers,dt) 
            self.dz        = fn(self.z,self.ztracers,dt)
            self.z         = self.z + self.dz[0]
            self.ztracers  = self.ztracers + self.dz[1]
            self.Z.append(self.z)
