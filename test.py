import unittest
import matplotlib.pyplot as plt
import numpy as np
import pdb
from numba import jit
from settings import *

# velocity without panels
# Decide panel strengths
# Compute new velocity on vortices
# Move vortex


class System():
    def __init__(self):

        self.z_vortices = np.array([])
        self.gamma_v    = np.array([])
        self.obj_z      = np.array([])
        self.obj_v      = 0.0
        self.obj_s      = np.array([])
        self.type       = np.array([])
        self.fs         = 1.0 + 0j

    def add_vortex(self,z, gamma,):
        
        self.z_vortices = np.append([self.z_vortices,z])
        self.gamma_v    = np.append([self.gamma_v, gamma])

    def computeA(self):

        self.cp  = (self.obj_z + np.roll(self.obj_z, -1))/2.0
        self.cpn = (self.obj_z - np.roll(self.obj_z, -1))
        self.cpn = 1j*self.cpn/np.absolute(self.cpn)
        self.A   = compute_A(self.obj_z, self.cp, self.cpn)
        return
    
    def computeV(self,z):
        return compute_vel(self.z_vortices, self.gamma_v, self.cp, self.fs, self.obj_z, self.obj_s)

    def computeB(self):
        v = compute_vel(self.z_vortices, self.gamma_v, self.cp, self.fs, np.array([]), np.array([]))\
                - self.obj_v
        self.B = np.real(v.conjugate()*self.cpn)
    
    def compute_panel_gamma(self):
        pass

def compute_A(panel_z, cps, cpns):
    n = panel_z.size
    A = np.zeros((n, n))
    for i,cp in enumerate(cps):
        for j,z in enumerate(panel_z):
            z1, z2, z3  =   panel_z[i-1], panel_z[i], panel_z[(i+1)%n]
            vel         =   panel_velocity(z1,z2,0,1,cp) + \
                            panel_velocity(z2,z3,1,0,cp)
            A[i,j] = np.real(vel.conjugate()*cpns[i])
    return A

@jit
def compute_vel(zv,gammav, cps, fs = 0.0+0.0j, zp = None, gammap = None):
    v_cps = np.zeros(cps.shape, dtype='complex128')
    for i,cp in enumerate(cps):
        for v,gamma in zip(zv,gammav):
            v_cps[i] = v_cps[i] + vortex_vel(cp-v, gamma)
        
        for j in xrange(zp.size):
            v_cps[i] = v_cps[i] + panel_velocity(zp[j-1], zp[j], gammap[j-1], gammap[j])

        v_cps[i] = v_cps[i] + fs
    return v_cps


X = np.linspace(-5,5,100)
Y = np.linspace(-5,5,100)
X, Y = np.meshgrid(X,Y)

theta = np.linspace(0,2*np.pi,20)
R = 1.0

sys = System()
sys.obj_z = R*(np.cos(theta) + 1.0j*np.sin(theta))
sys.obj_v = np.zeros_like(sys.obj_z)
sys.computeA()
import pdb; pdb.set_trace()
sys.computeB()
sys.obj_s = np.linalg.solve(sys.A, sys.B)
sys.computeV(-1.0+0.0j)

#V =     panel_velocity(0.0+1.0j, 1.0+0.0j,10,10,X+1j*Y) + \
#        panel_velocity(0.0+1.0j, 0.0+0.0j,10,10,X+1j*Y) + \
#        panel_velocity(0.0+0.0j, 1.0+0.0j,10,10,X+1j*Y)
##v = cent_pan_vel(X+1j*Y,1,1,2)
#
#plt.quiver(X,Y,np.real(V), np.imag(V))
#plt.show()
