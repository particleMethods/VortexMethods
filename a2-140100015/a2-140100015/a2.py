from main import *
from matplotlib import animation
import pdb
def integral_vorticity(y, b=1, gamma_0=1):
    temp =  1-4*np.square(y)/(b**2)
    return -1*gamma_0*np.sqrt(temp)

def net_vorticity(y1,y2,b=1,gamma_0=1, integralfn = integral_vorticity):
    return integralfn(y2) - integralfn(y1)


def display(S, savename):
    fig = plt.figure(figsize=(10,10), dpi=96)
    ax = plt.axes(xlim=(-2, 2), ylim=(-2, 2))
    point_line, = ax.plot([], [], 'r-x', lw=2, label = "Point")
    krasny_line, = ax.plot([], [], 'b-x', lw=2, label = "Krasny")
    lines = [point_line, krasny_line]
    plt.legend()
    def init():
        for line in lines:
            line.set_data([], [])
        return lines
    Xs, Ys = [], []
    for s in S:
        Xs.append(np.array([v.X for v in s.particles]))
        Ys.append(np.array([v.Y for v in s.particles]))

    # initialization function: plot the background of each frame
    def animate(i):
        for line,X,Y in zip(lines,Xs,Ys):
            x = X[:,i] 
            y = Y[:,i]
            line.set_data(x, y)
            #ax.set_xlim([x[-1]-0.25,x[-1]+0.25])
            #ax.set_ylim([y[-1]-0.25,y[-1]+0.25])
        return lines# + [ax]
    anim = animation.FuncAnimation(fig, animate, init_func=init, frames=200, interval=100, blit=True)
    anim.save(savename, fps=40, extra_args=['-vcodec', 'libx264'])

    plt.show()

def display2(S, savename):
    fig = plt.figure(figsize=(10,10), dpi=96)
    ax = plt.axes(xlim=(-0.5, 0.5), ylim=(0.2, 0.8))
    point_line, = ax.plot([], [], 'r-x', lw=2)
    krasny_line, = ax.plot([], [], 'b-x', lw=2)
    lines = [point_line, krasny_line]
    def init():
        for line in lines:
            line.set_data([], [])
        return lines
    Xs, Ys = [], []
    for s in S:
        Xs.append(np.array([v.X for v in s.particles]))
        Ys.append(np.array([v.Y for v in s.particles]))

    # initialization function: plot the background of each frame
    def animate(i):
        for line,X,Y in zip(lines,Xs,Ys):
            x = X[:,i] 
            y = Y[:,i]
            line.set_data(x, y)
        return lines

    anim = animation.FuncAnimation(fig, animate, init_func=init, frames=200, interval=100, blit=True)
    anim.save(savename, fps=40, extra_args=['-vcodec', 'libx264'])
    plt.show()
if __name__=='__main__':
    b    = 1.0
    N    = 100
    d    = 2.5*b/N

    y    = np.linspace(-b/2,b/2, N+1)
    vort = net_vorticity(y[:-1],y[1:])
    s = system([ vortex(0, (y[i]+y[i+1])/2, vort[i]) for i in xrange(N)])
    s2 = system([ vortex(0, (y[i]+y[i+1])/2, vort[i], blob_type="krasny", d=d) for i in xrange(N)])

    s.simulate(5e-3,1,"RK2")
    s2.simulate(5e-3,1,"RK2")

    display([s,s2], "a2a.mp4")
    display2([s,s2], "a2a2.mp4")

    ##Part2
    x    = np.linspace(-np.pi/2,np.pi/2, N+1)
    y    = b*np.sin(x)/2
    vort = net_vorticity(y[:-1],y[1:])
    s = system([ vortex(0.5*(x[i]+x[i+1]), (y[i]+y[i+1])/2, vort[i]) for i in xrange(N)])
    s2 = system([ vortex(0.5*(x[i]+x[i+1]), (y[i]+y[i+1])/2, vort[i], blob_type="krasny", d=d) for i in xrange(N)])

    s.simulate(5e-3,1,"RK2")
    s2.simulate(5e-3,1,"RK2")

    display([s,s2], "a2b.mp4")
    display2([s,s2], "a2b2.mp4")
