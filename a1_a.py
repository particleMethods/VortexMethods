import numpy as np
import matplotlib.pyplot as plt
import pdb

def vel(strength, xi= None, yi=None, x=None ,y=None ,alpha=None, type = "source"):
    if type == "source":
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        return strength*np.array([x,y])/(2*np.pi*r_sq)
    elif type == "uniform":
        out = np.zeros_like(x)
        out = np.append([out],[out],0)
        #TODO: Figure out a cleaner way
        #return np.reshape(strength*np.array([np.cos(alpha), np.sin(alpha)]),[2,1,1]) + np.zeros(out_size)
        out[0] = strength*np.cos(alpha)
        out[1] = strength*np.sin(alpha)
        return out
    elif type == "sink":
        return vel(-1*strength,xi, yi, x, y, alpha, type="source")
    elif type == "vortex":
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        return strength*np.array([-y,x])/(2*np.pi*r_sq)


def net_vel(x,y):
    return  vel(strength = 1, x=x,y=y, type = "uniform", alpha = 0) +\
            vel(strength=1, xi=-1, yi=0, x=x, y=y, type="source") +\
            vel(strength=1,xi=1, yi=0, x=x, y=y, type="sink")

def Phi(strength, xi= None, yi=None, x=None ,y=None ,alpha=None, type = "source"):
    if type == "vortex":
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        return strength*np.array([np.arctan(y/x), -np.log(np.sqrt(r_sq))])/(2*np.pi)
    if type == "source":
        x = x - xi; y = y - yi; r_sq = x**2 + y**2;
        return strength*np.array([np.log(np.sqrt(r_sq)),np.arctan(y/x)])/(2*np.pi)
    elif type == "uniform":
        #pdb.set_trace()
        z =  strength*(np.e**(-1j*alpha))*(x+1j*y)
        return np.array([np.real(z),np.imag(z)])
    elif type == "sink":
        return Phi(-1*strength,xi, yi, x, y, alpha, type="source")

def net_Phi(x,y):
    return  Phi(strength = 1,x=x,y=y, type = "uniform", alpha = 0) +\
            Phi(strength=1.0, xi=1.0, yi=0.0, x=x, y=y, type="sink") +\
            Phi(strength=1.0,xi=-1.0, yi=0.0, x=x, y=y, type="source")

def tracer(x, y,dt, Th, method="euler"):
    X = [x]; Y = [y]
    if method == "euler":
        for t in xrange(int(Th/dt)):
            dx, dy = dt*(net_vel(x,y))
            x, y = x + dx, y+dy
            X.append(x)
            Y.append(y)
    elif method == "RK4":
        for t in xrange(int(Th/dt)):
            k1x, k1y = net_vel(x,y)
            k2x, k2y = net_vel(x+dt*k1x/2, y+dt*k1y/2)
            k3x, k3y = net_vel(x+dt*k2x/2, y+dt*k2y/2)
            k4x, k4y = net_vel(x+dt*k3x, y+dt*k3y)

            x = x + dt*(k1x+2*k2x+2*k3x+k4x)/6
            y = y + dt*(k1y+2*k2y+2*k3y+k4y)/6

            X.append(x)
            Y.append(y)
    else:
        assert False

    return np.array([X,Y])

def plot_tracers():
    x = np.linspace(-2,-2,10)
    y = np.linspace(-2,2,10)

    X,Y = tracer(x,y, 1e-1, 10, method="RK4")

    for i in xrange(50):
        plt.plot(X[:,i],Y[:,i])

    plt.show()    


def part1():
    euler_errors = []
    rk_errors = []
    x, y       = -2.0, 0
    gt_x, gt_y = tracer(x,y, 1e-4,7, method="RK4")
    dt_set = np.linspace(1e-3, 2e-1,50)
    for dt in dt_set:
        e_x, e_y = tracer(x,y, dt,7, method="euler")
        r_x, r_y = tracer(x,y, dt,7, method="RK4")

        euler_errors.append((e_x[-1]-gt_x[-1])**2 + (e_y[-1]-gt_y[-1])**2)
        rk_errors.append((r_x[-1]-gt_x[-1])**2 + (r_y[-1]-gt_y[-1])**2)
    plt.plot(dt_set,euler_errors)
    plt.plot(dt_set,rk_errors)
    plt.show()

def part2():
        
#def net_vel(x,y):
#    return  vel(strength=1, xi=-0.5, yi=0, x=x, y=y, type="vortex") +\
#            vel(strength=1, xi=0.5, yi=0, x=x, y=y, type="vortex")
#
#def net_Phi(x,y):
#    return  Phi(strength = 1,x=x,y=y, type = "uniform", alpha = 0) +\
#            Phi(strength=1.0, xi=1.0, yi=0.0, x=x, y=y, type="sink") +\
#            Phi(strength=1.0,xi=-1.0, yi=0.0, x=x, y=y, type="source")
#x = np.linspace(-2.5,2.5,50) 
#y = np.linspace(-2.5,2.5,50) 
#x,y = np.meshgrid(x,y)

#dt = 0.01
#phi, psi = net_Phi(x,y)
#plt.contour(x,y,psi,50)
#plt.show()

#plt.contour(x,y,phi,50)
#plt.show()
