import numpy as np

nu              = 0.1
T               = 1.0
max_strength    = 0.01
strength        = 1.0
N               = np.ceil(strength/max_strength)
