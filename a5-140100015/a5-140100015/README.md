This code was tested and run on pypy (python2)
To run this code quickly, run it in a virtual environment where python points to pypy. numpy, jupyter and matplotlib need to be installed with pypy

To run simulations and generate report:
./run.sh
