import numpy as np

#Configuration
nu              = 0.1
T               = 1.0
max_strength    = 0.00001
strength        = 1.0
x1,y1           = -3.0, -3.0
x2,y2           = 3.0,3.0
h               = 0.25


N               = int(np.ceil(strength/max_strength))
gridnum         = int((x2-x1)/h) + 1
