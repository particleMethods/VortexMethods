import numpy as np
import matplotlib.pyplot as plt
import math
import cmath
import pdb;


class particle:
    def __init__(self, z, fixed=False):
        self.z = z
        self.fixed = fixed
        if not fixed:
            self.Z = [self.z]

    def get_vel(self,z):
        return 0

    def get_Phi(self, z):
        return 0

    def move(self, dz):
        if not self.fixed:
            self.z = self.z + self.dz
            self.Z.append(self.z)
        return

class tracer(particle):
    def get_vel(self, z):
        return 0

class uniform(particle):
    def __init__(self, strength = 1.0 + 0.0j, fixed=True):
        self.fixed = fixed
        self.strength = strength

    def get_vel(self, z):
        return self.strength
    def get_Phi(self, z):
        #TODO
        assert False
        pass

class vortex(particle):
    def __init__(self, z, strength=1, fixed=False, blob_type="point", d=0):
        particle.__init__(self,z,fixed)
        self.strength = strength
        self.blob_type = blob_type
        self.d = d
    def get_vel(self, z):
        if self.blob_type=="point":
            z = z - self.z
            if z == 0:
                return 0
            v = -1j*self.strength/(2*math.pi*z)
            return v.conjugate()
        elif self.blob_type=="krasny":
            z = z - self.z
            if z == 0:
                return 0

            x = np.real(z); y = np.imag(z); r_sq = x**2 + y**2;
            return self.strength*np.array(-y + 1j*x)/(2*np.pi*(abs(z)+self.d**2))
        else:
            assert False
    def get_Phi(self, z):
        z = z - zi
        #TODO
        assert False

def hatvel(l, z):
    if z == 0 or z == l:
        return 0
    v = -1j*(((z/l)-1)*cmath.log(1-l/z)+1)/(2*np.pi)
    return v.conjugate()

class control_point():
    def __init__(self, z, body, l, t):
        self.body = body
        self.z = z
        self.l = l
        self.t = t
        self.n = 1j*t
        body.cps.append(self)

class hat_panel(particle):
    def __init__(self,body,za,z,zb,gamma,fixed=False):
        self.gamma  = gamma
        self.body   = body
        self.za     = za
        self.zb     = zb
        self.z      = z
        self.fixed  = fixed
        la = za - z
        self.la     = math.sqrt(la.real**2 + la.imag**2)
        lb = zb - z
        self.lb     = math.sqrt(lb.real**2 + lb.imag**2)
        self.ta     = (z - za)/self.la
        self.tb     = (z - zb)/self.lb
        body.panels.append(self)

        #Jugaad
        control_point((za+z)/2.0, body, self.la, self.ta)


    def specific_velocity(self, z):
        la = self.la
        lb = self.lb
        ta = self.ta
        tb = self.tb
        z  = z - self.z
        try:
            return hatvel(la, z/ta)*ta + hatvel(lb, z/tb)*tb
        except:
            pdb.set_trace()

    def get_vel(self,z):
        return self.gamma*self.specific_velocity(z)
        

class const_panel(particle):
    def __init__(self,body,za,zb,gamma,fixed=False):
        self.gamma  = gamma
        self.body   = body
        self.za     = za
        self.zb     = zb
        self.fixed  = fixed
        l           = zb - za
        self.l      = math.sqrt(l.real**2 + l.imag**2)
        self.t      = (zb - za)/self.l
        body.panels.append(self)

        #Jugaad
        control_point((za+zb)/2.0, body, self.l, self.t)


    def specific_velocity(self, z):
        l = self.l
        t = self.t
        z  = (z - self.za)/t
        if z == 0 or z == l:
            return 0
        temp1 =(z/l -1)*(cmath.log(1-l/z)) + 1
        temp2 = (z/l)*cmath.log(1-l/z)+1
        v = -1j*(temp1 - temp2)/(2*np.pi)
        return v.conjugate()*t

    def get_vel(self,z):
        return self.gamma*self.specific_velocity(z)

class panel(particle):
    def __init__(self, body, z1=0.0 + 0.0j, z2 = 0.0+0.1j, gamma1=0.0, gamma2=0.0, fixed=False):
        self.body   = body
        self.z1     = z1
        self.z2     = z2
        self.zc     = (self.z1+self.z2)/2.0
        self.gamma1 = gamma1
        self.gamma2 = gamma2
        self.t      = self.z2 - self.z1
        self.l      = math.sqrt(self.t.real**2 + self.t.imag**2).item()
        self.t      = self.t/self.l
        self.n      = 1j*self.t
        self.fixed  = fixed
        body.panels.append(self)

    def get_vel(self, z):
        l = self.l
        z = (z-self.z1)/self.t
        temp1 = ((z/l) - 1)*cmath.log((z-l)/z) + 1
        temp2 = (z/l)*cmath.log(1-l/z) + 1
        v = -1j*(self.gamma1*temp1 - self.gamma2*temp2)/(2*math.pi)
        return v.conjugate()*self.t

class body:
    def __init__(self, v):
        self.v      = v
        self.panels = []
        self.cps    = []


class system:
    def __init__(self, particles=None, bodies=None, viscosity=0.1, max_vort=0.1):
        self.viscosity = viscosity
        self.particles=[]
        self.bodies=[]
        self.max_vort = max_vort

        if particles is not None:
            self.particles = particles
        if bodies is not None:
            self.bodies = bodies


    def add_particle(self, particle):
        self.particles.append(particle)
    
    def add_particles(self, particles):
        self.particles = self.particles + particles
    
    def add_body(self, body):
        self.bodies.append(body)
    
    def add_bodies(self, bodies):
        self.bodies = self.bodies + bodies

    def get_vel(self, z, inc_panel=False, src_particles = None, exc_particle=None):
        if src_particles is None:
            src_particles = self.particles
        if inc_panel:
            src_particles = self.particles + self.panels
        vel = 0
        #for particle in [particle for particle in self.particles if particle is not exc_particle]:
        for particle in src_particles:
            vel = vel + particle.get_vel(z)
#        for body in self.bodies:
#            for panel in body.panels:
#                vel = vel + panel.get_vel(z)
        return vel
    def computeA(self):
        a = []
        A = []
        n = 0
        panels = []
        control_points = []

        for body in self.bodies:
            panels = panels + body.panels
            control_points = control_points + body.cps

        self.control_points = control_points
        self.panels         = panels
        p, n = len(panels), len(control_points)
        for cp in control_points:
            for panel in panels:
                temp = panel.specific_velocity(cp.z).conjugate()*cp.n
                a.append(temp.real)
            A.append(a)
            a = []

        self.A = np.array(A)
        self.A = np.concatenate([self.A,np.ones([1,p])], axis=0)
    
    def computeB(self):
        b = []
        for cp in self.control_points:
            temp = (cp.body.v-1*self.get_vel(cp.z))*cp.n.conjugate()
            b.append(temp.real)
        b.append(0)
        self.B = np.array(b)

    def assignGammas(self):
        gammas = np.linalg.lstsq(self.A,self.B)[0].tolist()
        for i, panel in enumerate(self.panels):
            panel.gamma = gammas[i]

    def solvePanel(self):
        self.computeA()
        self.computeB()
        self.assignGammas()

    def get_Phi(self, z):
        #TODO
        assert False
        Phi = 0
        for particle in self.particles:
            Phi = Phi + particle.get_Phi(x, y)
        return Phi

    def get_moving_particles(self):
        return [particle for particle in self.particles if not particle.fixed]

    def get_tracers(self):
        return [particle for particle in self.particles if particle.__class__.__name__=="tracer"]
    def step(self, dt, method="euler"):
        #if method == "euler":
        #    for particle in self.get_moving_particles():
        #        particle.dx, particle.dy = dt*self.get_vel(particle.x, particle.y,particle)
        #    for particle in self.get_moving_particles():
        #        particle.move(particle.dx, particle.dy)

        #elif method == "RK4":
        #    for particle in self.get_moving_particles():
        #        x, y = particle.x, particle.y
        #        k1x, k1y = self.get_vel(x,y, particle)
        #        k2x, k2y = self.get_vel(x+dt*k1x/2, y+dt*k1y/2, particle)
        #        k3x, k3y = self.get_vel(x+dt*k2x/2, y+dt*k2y/2, particle)
        #        k4x, k4y = self.get_vel(x+dt*k3x, y+dt*k3y, particle)

        #        dx = dt*(k1x+2*k2x+2*k3x+k4x)/6
        #        dy = dt*(k1y+2*k2y+2*k3y+k4y)/6
        #        particle.dx, particle.dy = dx, dy

        #    for particle in self.get_moving_particles():
        #        particle.move(particle.dx, particle.dy)

        #elif method == "RK2":
            #Using midpoint method

        self.solvePanel()
        for particle in self.get_moving_particles():
            particle.temp_z = particle.z
            particle.dz = self.get_vel(particle.z, inc_panel=True)*dt/2

        for particle in self.get_moving_particles():
            particle.move(particle.dz)

        self.solvePanel()
        for particle in self.get_moving_particles():
            particle.dz = self.get_vel(particle.z, inc_panel=True)*dt

        for particle in self.get_moving_particles():
            particle.z = particle.temp_z + particle.dz

    def viscous_step(self, dt):
        for particle in self.particles:
            if particle.__class__.__name__=="vortex":
                if particle.strength>self.max_vort:
                    n = np.ceil(particle.strength/self.max_vort)
                    strength = particle.strength/n
                    typ, z, d = particle.type, particle.z, particle.d
                    self.particles.remove(particle)
                    for i in xrange(n):
                        v = vortex(z, strength, False, typ, d)
                        self.add_particle(v)
                else:
                    v1,v2 = np.random.normal(0,2*np.sprt(2*self.viscous_step*dt), 2)
                    particle.move(v1+v2*1j)

        for particle in self.get_moving_particles():
            particle.temp_z = particle.z
            particle.dz = self.get_vel(particle.z)*dt/2

        for particle in self.get_moving_particles():
            particle.move(particle.dz)

        for particle in self.get_moving_particles():
            particle.dz = self.get_vel(particle.z)*dt

        for particle in self.get_moving_particles():
            particle.z = particle.temp_z + particle.dz


    def simulate(self, dt, Th, method="euler"):

        tsteps = int(Th/dt)
        for n in xrange(tsteps):
            self.viscous_step(dt )

