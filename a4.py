import matplotlib
#matplotlib.use('Agg')
#matplotlib.use('GTKAgg') 
from main import *

def solve1(n = 100,typ="constant"):    
    thetas = np.linspace(0,2*np.pi,n+1)[:-1]
    z = np.cos(thetas) + 1j*np.sin(thetas)
    za = np.roll(z,-1).tolist()
    zb = np.roll(z, 1).tolist()
    z  = z.tolist()

    b = body(v=0.0+0.0j)
    if typ == "constant":
        panels = [const_panel(b, zb[i], z[i], 0.0) for i in xrange(n)]
    elif typ == "linear":
        panels = [hat_panel(b, za[i], z[i], zb[i], 0.0) for i in xrange(n)]
    s = system(bodies = [b])
    s.add_particle(uniform())

    s.solvePanel()
    return s

s = solve1()
x = np.linspace(-5,5,10)
y = np.linspace(5,-5,10)
X,Y = np.meshgrid(x,y)
Z = X + 1j*Y
Z = Z.tolist()
V = np.zeros([10,10], dtype='complex128').tolist()
for i in xrange(10):
    for j in xrange(10):
        V[i][j] = s.get_vel(Z[i][j],inc_panel=True)

plt.quiver(X,Y,np.real(V), np.imag(V))
plt.show()

N = 10
def vr(r, theta, R=1.0):
    return 1*(1-(R/r)**2)*math.cos(theta)
def vt(r, theta, R=1.0):
    return -1*(1+(R/r)**2)*math.sin(theta)

R = np.linspace(1,5,5).tolist()
Theta = np.linspace(0,2*np.pi, 200).tolist()

vsim = []
vth  = []
X = []
Y = []

s = solve1()
err = np.zeros([200,5]).tolist()

for i,r in enumerate(R):
    for j,theta in enumerate(Theta):
        x = r*math.cos(theta)
        y = r*math.sin(theta)
        X.append(x)
        Y.append(Y)
        vsim.append(abs(s.get_vel(x+1j*y)))
        vth.append(abs(vr(r,theta) +1j*vt(r,theta)))
        err[j][i] = vsim[-1]-vth[-1]

R,Theta = np.meshgrid(R,Theta)

fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
ax.contourf(Theta,R,err)
plt.show()
